from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator
from django.db.models import Count
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from .models import Blog, BlogType
from comment.models import Comment
#引入(read_statistics)app里的通用方法read_statistics_once_read
from read_statistics.utils import read_statistics_once_read

def get_blog_common_data(request, blogs_all_list):
    # 每BLOGS_PER_PAGE条博客分页
    paginator = Paginator(blogs_all_list, settings.BLOGS_NUM_PER_PAGE)
    # 获取url的页面参数
    page_num = request.GET.get('page', 1)
    page_of_blogs = paginator.get_page(page_num)
    page_range = [x for x in range(int(page_num) - 2, int(page_num) + 3) if 0 < x <= paginator.num_pages]
    # 加上首页与尾页
    if page_range[0] != 1:
        page_range.insert(0, 1)
    if page_range[-1] != paginator.num_pages:
        page_range.append(paginator.num_pages)

    context = {}
    context['blogs'] = page_of_blogs.object_list
    # 总页码
    context['page_of_blogs'] = page_of_blogs
    # 过滤后页码
    context['page_range'] = page_range
    # 备注并获取博客分类对应的数量
    context['blog_types'] = BlogType.objects.annotate(type_count=Count('blog'))
    context['blog_dates'] = Blog.objects.dates('created_time', 'month', order='DESC')
    return context

def blog_list(request):
    blogs_all_list = Blog.objects.all()
    context = get_blog_common_data(request, blogs_all_list)
    return render(request, 'blog_list.html', context)

def blogs_with_type(request, blog_with_pk):
    blog_type = get_object_or_404(BlogType, pk=blog_with_pk)
    blogs_all_list = Blog.objects.filter(blog_type=blog_type)
    context = get_blog_common_data(request, blogs_all_list)
    context['blog_type'] = blog_type
    return render(request, 'blog_with_type.html', context)

def blogs_with_date(request, year, month):
    blogs_all_list = Blog.objects.filter(created_time__year=year, created_time__month=month)
    context = get_blog_common_data(request, blogs_all_list)
    context['blogs_with_date'] = '%s年%s月' %(year, month)
    return render(request, 'blog_with_date.html', context)

def blog_detail(request, blog_pk):
    blog = get_object_or_404(Blog, pk=blog_pk)
    #若无COOKIES,访问+1
    read_cookie_key = read_statistics_once_read(request, blog)
    blog_content_type = ContentType.objects.get_for_model(blog)
    comments = Comment.objects.filter(content_type=blog_content_type, object_id=blog.pk)

    context = {}
    context['previous_blog'] = Blog.objects.filter(created_time__gt=blog.created_time).last()
    context['next_blog'] = Blog.objects.filter(created_time__lt=blog.created_time).first()
    context['blog'] = blog
    context['comments'] = comments
    response = render(request, 'blog_detail.html', context)
    #设置COOKIE缓存
    response.set_cookie(read_cookie_key,'True')
    return response