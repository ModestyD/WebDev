from django.db import models
from django.db.models.fields import exceptions
from django.contrib.auth.models import User
#引入ContentType
from django.contrib.contenttypes.models import ContentType
#引入(read_statistics)app里的ReadNUM模型
from read_statistics.models import ReadNum, ReadNumMethod
from ckeditor_uploader.fields import RichTextUploadingField
class BlogType(models.Model):
    #类目名称
    type_name = models.CharField(max_length=30)

    def __str__(self):
        return self.type_name

class Blog(models.Model, ReadNumMethod):
    # 博客类型
    blog_type = models.ForeignKey(BlogType, on_delete=models.DO_NOTHING)
    # 博客标题
    title = models.CharField(max_length=50)
    # 博客正文
    content = RichTextUploadingField()
    # 博客作者
    author = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    # 博客创建时间
    created_time = models.DateTimeField(auto_now_add=True)
    # 博客最后修改时间
    last_updated_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "<Blog: %s>" % self.title

    class Meta:
        ordering = ['-created_time']