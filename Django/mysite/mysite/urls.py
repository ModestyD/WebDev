"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from . import views
urlpatterns = [
    #首页路由
    path('',views.home,name='home'),
    #后台管理路由
    path('admin/', admin.site.urls),
    #blog_app路由
    path(r'blog/', include('blog.urls')),
    #editor_app路由
    path('ckeditor/', include('ckeditor_uploader.urls')),
    #comment路由
    path('comment/', include('comment.urls')),
    #loggin路由
    path('login/', views.login, name='login'),
    #register路由
    path('register/', views.register, name='register'),
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
