from django.shortcuts import render, redirect
from read_statistics.utils import get_week_read_data
from django.contrib.contenttypes.models import ContentType
from blog.models import Blog
from django.contrib import auth
from django.contrib.auth.models import User
from django.urls import reverse
from .forms import LoginForm, RegForm

def home(request):
    blog_content_type = ContentType.objects.get_for_model(Blog)
    read_nums, dates_showhome = get_week_read_data(blog_content_type)
    context = {
        'read_nums':read_nums,
        'dates_showhome':dates_showhome,
    }
    return render(request, 'home.html', context=context)

def login(request):
    if request.method == 'POST':
        login_form = LoginForm(request.POST)
        #验证传入服务器用户名与密码表单是否有效
        if login_form.is_valid():
            user = login_form.cleaned_data['user']
            auth.login(request, user)
            return redirect(request.GET.get('from', reverse('home')))
    else:
        login_form = LoginForm()

    context={}
    context['login_form'] = login_form
    return render(request, 'login.html',context)

def register(request):
    if request.method == 'POST':
        reg_form = RegForm(request.POST)
        # 验证传入服务器用户名与密码表单是否有效
        if reg_form.is_valid():
            username = reg_form.cleaned_data['username']
            email = reg_form.cleaned_data['email']
            password = reg_form.cleaned_data['password']
            #创建用户之后自动登录并返回上一页面
            user = User.objects.create_user(username, email, password)
            user.save()
            user = auth.authenticate(username=username, password=password)
            auth.login(request, user)
            return redirect(request.GET.get('from', reverse('home')))
    else:
        reg_form = RegForm()

    context = {}
    context['reg_form'] = reg_form
    return render(request, 'register.html', context)