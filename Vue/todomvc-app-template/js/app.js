
var app = new Vue({
	el:todoapp,
	data:{
		user_input:"",
		// 加载本地存储数据
		list_todos:JSON.parse(localStorage.getItem('list_todos')) || [],
		all_sign:true
	},
	methods:{
		add_todo:function () {
		let text = this.user_input && this.user_input.trim();
		if(!text)
			return;
		this.list_todos.push({
			content: text,
			id: this.list_todos.length,
			completed:false,
			isshow:true
		});
		this.user_input="";
		},
		remove_todo:function (it) {
			this.list_todos.splice(it,1);
		},
		completed_all:function () {
			if (this.all_sign){
				for(let i=0;i<this.list_todos.length;i++){
					this.list_todos[i].completed=true;
				}
			}
			else{
				for(let i=0;i<this.list_todos.length;i++){
					this.list_todos[i].completed=false;
				}
			}
			// 将全部操作信号取反
			this.all_sign = !this.all_sign;
		},
		completed_this:function (index) {
			this.list_todos[index].completed = !this.list_todos[index].completed;
		},
		clear_all:function () {
			for (let i=0;i<this.list_todos.length;i++){
				console.log("index"+i)
				console.log("数组长度"+this.list_todos.length)
				if (this.list_todos[i].completed==true){
					console.log("deleteindex"+i)
					// 这个地方涉及操作data长度，index需要在删除操作后减1,否则删除当前index后，循环会从当前index继续开始遍历，导致跳过某一个index值
					this.list_todos.splice(i,1);
					i -= 1;
				}
			}
		},
		// 可显属性全设为真
		filter_all:function () {
			for (let i=0;i<this.list_todos.length;i++){
				this.list_todos[i].isshow=true;
			}
		},
		// 根据完成状态过滤可显属性，当已完成时设置可显为假！循环第一时间一定要重置可显属性值,否则当从完成状态与未完成状态切换显示状态时会出现异常
		filter_active:function () {
			for (let i=0;i<this.list_todos.length;i++){
				this.list_todos[i].isshow=true;
				if (this.list_todos[i].completed==true){
						this.list_todos[i].isshow=false;
				}
			}
		},
		filter_completed:function () {
			for (let i=0;i<this.list_todos.length;i++){
				this.list_todos[i].isshow=true;
				if (this.list_todos[i].completed==false){
						this.list_todos[i].isshow=false;
				}
			}
		}
	},
	//定义一个可以过滤数据返回状态为未完成对象数量的过滤器
	filters: {
		filter_uncomplete:function (values) {
			let num=0;
			for (let i=0;i<values.length;i++){
				if (values[i].isshow == true){
					num += 1;
				}
			}
			return num;
		}
	},
	// 设置计算属性，当方法依赖的list_todos数组内完成状态属性发生变化的时候，返回变化值
	computed:{
		now_remaning:function () {
			let remaning_num = this.list_todos.length
			for (let i=0;i<this.list_todos.length;i++){
				if (this.list_todos[i].completed == true){
					remaning_num --;
					// 一旦检测发现变化就不用再遍历了
					return remaning_num
				}
			}
		}
	},
	// 设置监听器，将list_todos数据本地持久化
	watch:{
		list_todos:{
			// 对象内属性值一旦发生变化就保存新的数据
			deep:true,
			handler(newVal){
				console.log('发生变化了'+newVal);
				// 字符化
				localStorage.setItem('list_todos', JSON.stringify(newVal));
				console.log(localStorage.getItem('list_todos'));
			}
		}
	}
})
